<?php 
require 'config.php';
require 'system/database.php';
function testSortingOrder($dbData,$pageData){
	$returnFlag=0;
  if(!empty($dbData) && !empty($pageData)){
  	if(count($dbData) &&  count($pageData)){
  		foreach($dbData as $key=>$data){
  			if($data['username']==$pageData[$key]){
  				$returnFlag=1;
  			}else{
  				$returnFlag=0;
  			}
  		}
  	}
  }
  return $returnFlag;
}

$I = new AcceptanceTester($scenario);
$I->wantTo('compare the users sortable data');
$I->amOnPage('/users'); 
$I->fillField('username', 'demo');
$I->fillField('password', 'demo');
$I->click('login_btn');
$I->amOnPage('/users');
$current_sort=$I->grabAttributeFrom('.sortable.full_name', 'data-sort');
$column=$I->grabAttributeFrom('.sortable.full_name', 'data-column');
$I->click('.sortable.full_name');// Firing the click event 
$sort=($current_sort=='asc') ? 'DESC' : "ASC"; 
$query="SELECT * FROM users WHERE deleted=0";
$query.=" order by $column $current_sort";
$AllusersfromDB=get_all($query);
$I->amOnPage('/users');
$userDatafromPage = $I->grabMultiple('.users .username');
$testFlag=testSortingOrder($AllusersfromDB,$userDatafromPage);
\PHPUnit_Framework_Assert::assertTrue($testFlag==1);



